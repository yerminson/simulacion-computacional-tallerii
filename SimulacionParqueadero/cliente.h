#ifndef CLIENTE_H
#define CLIENTE_H

#include <string>

class Cliente
{
private:
    int tiempo;
    std::string estado;
    int id;
    int parqueadero;
    int costo;
public:
    Cliente(std::string estado, int tiempo, int id, int parqueadero, int costo);
    std::string getEstado();
    int getId();
    int getParqueadero();
    int getCosto();
    int getTiempo() const;
    void setEstado(std::string estado);
    void setId(int id);
    void setParqueadero(int parqueadero);
    void setCosto(int costo);
    void setTiempo(int tiempo);

    bool operator>(const Cliente &otro) const;
};

#endif // CLIENTE_H
