#ifndef PARQUEADERO_H
#define PARQUEADERO_H

#include <queue>
#include <vector>
#include "cliente.h"

class Parqueadero
{
private:
    int L;
    int P;
    int K;
    std::vector<std::vector<int> > distancias;
    std::priority_queue< Cliente, std::vector<Cliente>, std::greater<Cliente> > cola;
    int ps; //parqueadero seleccionado
    int costo; //lo que le cuesta a un cliente estaciona en un parqueadero
    bool *pLibres; //parqueaderos libres
    int idConsecutivo;

    void continuarSimulacion();
    void ejecutarEvento();
    void calcularCosto(std::vector<int> localesParaVisitar, int cantidadLocales);

public:
    Parqueadero(int L, int P, int K, std::vector<std::vector<int> > distancias);
    ~Parqueadero();
    void llegada(std::vector<int> eventoIngreso);
};

#endif // PARQUEADERO_H
