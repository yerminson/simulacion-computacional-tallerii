#ifndef COSTOPARQUEADERO_H
#define COSTOPARQUEADERO_H

class CostoParqueadero
{
private:
    int parqueadero;
    int costo;
public:
    CostoParqueadero(int parqueadero, int costo);
    int getCosto() const;
    int getParqueadero();
    void setCosto(int costo);
    void setParqueadero(int parqueadero);

    bool operator<(const CostoParqueadero &otro) const;
};

#endif // COSTOPARQUEADERO_H
