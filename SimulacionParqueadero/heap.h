#ifndef HEAP_H
#define HEAP_H

#include "element.h"

class Heap
{
    int heapSize;
    int size;
    Element *array;
public:
    Heap(int size, Element *array, int n);
    void heapify(int nodo);
    void buildHeap();
    void copyArray();
    void heapExtract();
    void heapInsert(Element elem);


};

#endif // HEAP_H
