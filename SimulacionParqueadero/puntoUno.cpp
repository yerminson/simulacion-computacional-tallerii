#include<iostream>
#include"heap.h"

using namespace std;

int main()
{
    int n;
    cin>>n;

    Element* array= new Element[n];
    for(int i=0; i<n;i++)
    {
        int priority;
         cin>> priority;
        string name;
        cin>> name;

        array[i]=Element(priority, name);
    }

    Heap *heap = new Heap(1000000, array, n);
    heap->buildHeap();
    heap->copyArray();


    int o;
    cin>>o;
    for(int i=0; i<o; i++)
    {
        string action;
        cin>>action;

        if(action=="E")
        {
            heap->heapExtract();
        }else{
            int priority;
            cin>> priority;
            string name;
            cin>> name;
            Element ele(priority, name);

            heap->heapInsert(ele);

        }

        heap->copyArray();
    }

    delete heap;
    return 0;
}
